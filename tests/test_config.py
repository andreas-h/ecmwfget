import datetime
import os.path
import yaml

from .context import ecmwfget

TESTDATA = 'config.yaml'
TESTDATA = os.path.join(os.path.dirname(__file__), '..', TESTDATA)


def test_load_config():
    cfg = ecmwfget.get.load_config(TESTDATA, 'IFS')
    return cfg


def test_construct_requests():
    cfg = ecmwfget.get.load_config(TESTDATA, 'IFS')


    actual = ecmwfget.get.construct_requests(
        datetime.datetime(2017, 5, 10), cfg, 'ecmwf_{date:%Y-%m-%d}_{type}_{id}.grb', data='WRF_ML', static_fields=True)

    return actual
